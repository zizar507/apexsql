SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_1] (
		[col1]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col2]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col3]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col4]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col5]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col6]     [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Table_1] SET (LOCK_ESCALATION = TABLE)
GO
