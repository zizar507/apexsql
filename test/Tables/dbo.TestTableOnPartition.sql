SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TestTableOnPartition] (
		[InternalRuleId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[RuleId]             [uniqueidentifier] NOT NULL,
		CONSTRAINT [PK]
		PRIMARY KEY
		CLUSTERED
		([InternalRuleId])
	ON [test_s] ([InternalRuleId])
) ON [test_s] ([InternalRuleId])
GO
ALTER TABLE [dbo].[TestTableOnPartition] SET (LOCK_ESCALATION = TABLE)
GO
